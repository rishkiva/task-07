package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;

import javax.sql.DataSource;


public class DBManager {

	private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";
	private static final String URL = "jdbc:mysql://localhost:3306/task6?user=root&password=22andriy";
	private static final String GET_ALL_USERS = "SELECT * FROM users";
	private static final String GET_ALL_TEAMS = "SELECT * FROM teams";
	private static final String GET_ALL_TEAMS_USER = "SELECT login, teams.id, name FROM users inner join (teams INNER JOIN USERS_TEAMS ON teams.id = users_teams.team_id) on user_id = users.id where login like ?";
	private static final String INSERT_USER = "insert into users values (DEFAULT, ?)";
	private static final String DELETE_USER = "DELETE FROM users WHERE id = ?";
	private static final String DELETE_TEAM = "DELETE FROM teams WHERE id = ?";
	private static final String DELETE_TEAM_USER = "DELETE FROM users_teams WHERE team_id = ?";
	private static final String DELETE_USER_TEAM = "DELETE FROM users_teams WHERE user_id = ?";
	private static final String INSERT_TEAM = "insert into teams values (DEFAULT, ?)";
	private static final String INSERT_USER_TEAM = "insert into users_teams values (?, ?)";
	private static final String SELECT_USER = "SELECT * FROM users WHERE login like ?";
	private static final String SELECT_TEAM = "SELECT * FROM teams WHERE name like ?";

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private Connection getConnection(){
		Connection con = null;
		try {
			con = DriverManager.getConnection(CONNECTION_URL);
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return con;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(GET_ALL_USERS);
			while (rs.next()) {
				User user = User.createUser(rs.getString("login"));
				user.setId(rs.getInt("id"));
				users.add(user);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			close(con, stmt, rs);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			stmt = con.prepareStatement(INSERT_USER);
			stmt.setString(1,user.getLogin());
			stmt.executeUpdate();
			return true;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return false;
		} finally {
			close(con, stmt, rs);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			for (User user:users) {
				stmt = con.prepareStatement(DELETE_USER);
				stmt.setInt(1,user.getId());
				stmt.executeUpdate();
				stmt = con.prepareStatement(DELETE_USER_TEAM);
				stmt.setInt(1,user.getId());
				stmt.executeUpdate();
			}
			return true;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return false;
		} finally {
			close(con, stmt, rs);
		}
	}

	public User getUser(String login) throws DBException {
		User user = null;
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			stmt = con.prepareStatement(SELECT_USER);
			stmt.setString(1,login);
			stmt.execute();
			rs = stmt.executeQuery();
			rs.next();
			user = User.createUser(rs.getString("login"));
			user.setId(rs.getInt("id"));
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			close(con, stmt, rs);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			stmt = con.prepareStatement(SELECT_TEAM);
			stmt.setString(1,name);
			stmt.execute();
			rs = stmt.executeQuery();
			rs.next();
			team = Team.createTeam(rs.getString("name"));
			team.setId(rs.getInt("id"));
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			close(con, stmt, rs);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(GET_ALL_TEAMS);
			while (rs.next()) {
				Team team = Team.createTeam(rs.getString("name"));
				team.setId(rs.getInt("id"));
				teams.add(team);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			close(con, stmt, rs);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			stmt = con.prepareStatement(INSERT_TEAM);
			stmt.setString(1,team.getName());
			stmt.executeUpdate();
			return true;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return false;
		} finally {
			close(con, stmt, rs);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {

		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			for (Team team:teams) {
				stmt = con.prepareStatement(INSERT_USER_TEAM);
				stmt.setInt(1,user.getId());
				stmt.setInt(2,team.getId());
				stmt.executeUpdate();
			}
			con.commit();
			return true;
		} catch (SQLException ex) {
			ex.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException throwables) {
				throwables.printStackTrace();
			}
			return false;
		} finally {
			close(con, stmt, rs);
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			stmt = con.prepareStatement(GET_ALL_TEAMS_USER);
			stmt.setString(1,user.getLogin());
			rs = stmt.executeQuery();
			while (rs.next()) {
				Team team = Team.createTeam(rs.getString("name"));
				team.setId(rs.getInt("id"));
				teams.add(team);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			close(con, stmt, rs);
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			stmt = con.prepareStatement(DELETE_TEAM);
			stmt.setInt(1,team.getId());
			stmt.executeUpdate();
			stmt = con.prepareStatement(DELETE_TEAM_USER);
			stmt.setInt(1,team.getId());
			stmt.executeUpdate();
			return true;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return false;
		} finally {
			close(con, stmt, rs);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection con = getConnection();
		return false;
	}

	private void close(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	private void close(Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

 	void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	private void close(Connection con, Statement stmt, ResultSet rs) {
		close(rs);
		close(stmt);
		close(con);
	}

}
